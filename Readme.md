# Readme

## How to install
* Add private repository to your composer.json:
```
...
"repositories": [
     {
         "type": "vcs",
         "url":  "git@bitbucket.org:Crea17/mysql_logger.git"
     }
],
...
```
* Add logger_parameters.yml to your project's root directory:
```
parameters:
    name: ~
    host: ~
    user: ~
    password: ~
    table: ~
```
* Add composer scripts:
```
...
"scripts": {
    "post-install-cmd": [
        "Gstawarczyk\\Logger\\Script\\Install::install"
    ],
    "post-update-cmd": [
        "Gstawarczyk\\Logger\\Script\\Install::install"
    ]
},
...
```
* Run ```$ composer require gstawarczyk/mysql_logger ```