<?php

declare(strict_types=1);

namespace Gstawarczyk\Logger\Exception;

use Exception;
use Throwable;

/**
 * @author Grzegorz Stawarczyk <grzegorz.stawarczyk@gmail.com>
 */
class ParsingParametersException extends Exception
{
    /**
     * @param Throwable|null $previous
     */
    public function __construct(Throwable $previous = null)
    {
        $message = 'An error occurred when parsing parameters.yml';
        parent::__construct($message, 0, $previous);
    }
}