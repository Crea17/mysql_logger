<?php

declare(strict_types=1);

namespace Gstawarczyk\Logger\Factory;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOMySql\Driver;
use Gstawarczyk\Logger\Parameters\Configuration;

class ConnectionFactory
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return Connection
     * @throws DBALException
     */
    public function create(): Connection
    {
        return new Connection([
            'dbname' => $this->configuration->getName(),
            'user' => $this->configuration->getUser(),
            'password' => $this->configuration->getPassword(),
            'host' => $this->configuration->getHost(),
        ], new Driver());
    }
}
