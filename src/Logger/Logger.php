<?php

declare(strict_types=1);

namespace Gstawarczyk\Logger\Logger;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Gstawarczyk\Logger\Factory\ConnectionFactory;
use Gstawarczyk\Logger\Parameters\Configuration;
use Gstawarczyk\Logger\Parameters\ParametersParser;
use Throwable;

final class Logger
{
    /**
     * @var Connection
     */
    private static $connection;

    /**
     * @var string
     */
    private static $table;

    /**
     * @param Configuration $configuration
     */
    public static function init(Configuration $configuration): void
    {
        ParametersParser::setParameters($configuration);
    }

    /**
     * @param string $type
     * @param array $data
     *
     * @throws DBALException
     */
    public static function log(string $type, array $data): void
    {
        try {
            self::connect();

            $query = 'INSERT INTO ' . self::$table . ' (`type`, `data`) VALUES (:type, :data)';
            $params = [
                'type' => $type,
                'data' => json_encode($data),
            ];
            $paramTypes = [
                'type' => \PDO::PARAM_STR,
                'data' => \PDO::PARAM_STR,
            ];

            self::$connection->executeQuery($query, $params, $paramTypes);
        } catch (Throwable $th) {
            // Do nothing
        }
    }

    /**
     * @return void
     * @throws DBALException
     */
    private static function connect(): void
    {
        if (self::$connection instanceof Connection && self::$connection->ping()) {
            return;
        }

        self::createConnection();
    }

    /**
     * @return void
     * @throws DBALException
     */
    private static function createConnection(): void
    {
        $config = ParametersParser::getParams();
        self::$table = $config->getTable();
        self::$connection = (new ConnectionFactory($config))->create();
    }
}
