<?php

declare(strict_types=1);

namespace Gstawarczyk\Logger\Parameters;

/**
 * @author Grzegorz Stawarczyk <grzegorz.stawarczyk@gmail.com>
 */
class Configuration
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $table;

    /**
     * @param string $host
     * @param string $name
     * @param string $user
     * @param string $password
     * @param string $table
     */
    public function __construct(
        string $host,
        string $name,
        string $user,
        string $password,
        string $table
    ) {
        $this->host = $host;
        $this->name = $name;
        $this->user = $user;
        $this->password = $password;
        $this->table = $table;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }
}