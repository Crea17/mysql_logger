<?php

declare(strict_types=1);

namespace Gstawarczyk\Logger\Parameters;

use Gstawarczyk\Logger\Exception\ParsingParametersException;
use Symfony\Component\Yaml\Yaml;

final class ParametersParser
{
    /**
     * @var Configuration
     */
    private static $parameters;

    /**
     * @return Configuration
     * @throws ParsingParametersException
     */
    public static function getParams(): Configuration
    {
        if (!\is_array(self::$parameters)) {
            $parameters = Yaml::parse(file_get_contents(__DIR__ . '/../../../../../logger_parameters.yml'));
            if (!isset($parameters['parameters'])) {
                throw new ParsingParametersException();
            }
            self::$parameters = new Configuration(
                $parameters['parameters']['host'],
                $parameters['parameters']['name'],
                $parameters['parameters']['user'],
                $parameters['parameters']['password'],
                $parameters['parameters']['table']
            );
        }

        return self::$parameters;
    }

    /**
     * @param Configuration $configuration
     */
    public static function setParameters(Configuration $configuration): void
    {
        self::$parameters = [
            'host' => $configuration->getHost(),
            'name' => $configuration->getName(),
            'user' => $configuration->getUser(),
            'password' => $configuration->getPassword(),
            'table' => $configuration->getTable()
        ];
    }
}
