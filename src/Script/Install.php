<?php

declare(strict_types=1);

namespace Gstawarczyk\Logger\Script;

use Composer\Script\Event;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Gstawarczyk\Logger\Factory\ConnectionFactory;
use Gstawarczyk\Logger\Parameters\ParametersParser;

class Install
{
    /**
     * @param Event $event
     *
     * @throws DBALException
     */
    public static function install(Event $event): void
    {
        $config = ParametersParser::getParams();
        $connection = (new ConnectionFactory($config))->create();

        $table = $config->getTable();
        $con = $connection->executeQuery('SHOW TABLES LIKE \'' . $table . '\'');

        if ($con->fetch() === false) {
            self::createTable($connection, $table);

            return;
        }

        echo 'Table ' . $table . ' exists. Skipping' . PHP_EOL;
    }

    /**
     * @param Connection $connection
     * @param string $tableName
     *
     * @throws DBALException
     */
    private static function createTable(Connection $connection, string $tableName): void
    {
        echo 'Creating Table ' . $tableName . PHP_EOL;
        $connection->executeQuery(
            "CREATE TABLE {$tableName} (
                  id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                  type VARCHAR(255) NOT NULL,
                  data JSON NOT NULL,
                  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
                )"
        );
    }
}
